package se325.lab03.concert.domain;

import javax.persistence.Embeddable;

public enum Genre {
    Pop, HipHop, RhythmAndBlues, Acappella, Metal, Rock
}