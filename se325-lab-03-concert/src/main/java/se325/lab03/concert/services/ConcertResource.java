package se325.lab03.concert.services;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import se325.lab03.concert.common.Config;
import se325.lab03.concert.domain.Concert;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to implement a simple REST Web service for managing Concerts.
 * <p>
 * ConcertResource implements a WEB service with the following interface:
 * - GET    <base-uri>/concerts/{id}
 * Retrieves a Concert based on its unique id. The HTTP response
 * message has a status code of either 200 or 404, depending on
 * whether the specified Concert is found.
 * <p>
 * - GET    <base-uri>/concerts?start&size
 * Retrieves a collection of Concerts, where the "start" query
 * parameter identifies an index position, and "size" represents the
 * max number of successive Concerts to return. The HTTP response
 * message returns 200.
 * <p>
 * - POST   <base-uri>/concerts
 * Creates a new Concert. The HTTP post message contains a
 * representation of the Concert to be created. The HTTP Response
 * message returns a Location header with the URI of the new Concert
 * and a status code of 201.
 * <p>
 * - DELETE <base-uri>/concerts
 * Deletes all Concerts, returning a status code of 204.
 * <p>
 * Where a HTTP request message doesn't contain a cookie named clientId
 * (Config.CLIENT_COOKIE), the Web service generates a new cookie, whose value
 * is a randomly generated UUID. The Web service returns the new cookie as part
 * of the HTTP response message.
 */
@Path("/concerts")
public class ConcertResource {

    private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);

    private Map<Long, Concert> concertDB = new ConcurrentHashMap<>();
    private AtomicLong idCounter = new AtomicLong();

    @GET
    @Path("{id}")
    @Produces({
            javax.ws.rs.core.MediaType.APPLICATION_JSON,
            SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT
    })
    public Response retrieveConcert(@PathParam("id") long id, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
        // if newCookie is null, it means the client had specified a cookie, so it doesn't need to be included in the reponse
        NewCookie newCookie = makeCookie(clientId);

        LOGGER.info("Retrieving Concert with id: " + id);

        Concert concert = concertDB.get(id);
        if(concert == null){
            // Concert with this ID doesn't exist, return HTTP 404 Response
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        // The concert was found, return 200 success along with the Concert object
        // if newCookie is null, this means no cookies will be sent in the response
        //      which is fine because null indicates the client already had a cookie.
        return Response.ok(concert).status(Response.Status.OK).cookie(newCookie).build();
    }

    @GET
    @Produces({
            javax.ws.rs.core.MediaType.APPLICATION_JSON,
            SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT
    })
    public Response retrieveConcerts(@QueryParam("start") long start, @QueryParam("size") int size, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
        // The Response object should store an ArrayList<Concert> entity. The
        // ArrayList can be empty depending on the start and size arguments,
        // and Concerts stored.
        //
        // Because of type erasure with Java Generics, any generically typed
        // entity needs to be wrapped by a javax.ws.rs.core.GenericEntity that
        // stores the generic type information. Hence to add an ArrayList as a
        // Response object's entity, you should use the following code:
        //
        // List<Concert> concerts = new ArrayList<Concert>();
        // GenericEntity<List<Concert>> entity = new GenericEntity<List<Concert>>(concerts) {};
        // ResponseBuilder builder = Response.ok(entity);
        NewCookie newCookie = makeCookie(clientId);

        LOGGER.info("Retrieving list of all concerts, starting with " + start + " with max size of " + size);

        List<Concert> concerts = new ArrayList<>();

        List<Long> availableIds = new ArrayList<>(concertDB.keySet());
        Collections.sort(availableIds);
        int position = availableIds.indexOf(start);

        for(int i = position; i < availableIds.size() && concerts.size() < size; i++){
            if(concertDB.containsKey(availableIds.get(i))){
                concerts.add(concertDB.get(availableIds.get(i)));
            }
        }

        GenericEntity<List<Concert>> entity = new GenericEntity<>(concerts) {
        };
        return Response.ok(entity).status(Response.Status.OK).cookie(newCookie).build();
    }

    @POST
    @Consumes({
            javax.ws.rs.core.MediaType.APPLICATION_JSON,
            SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT
    })
    public Response createConcert(Concert concert, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {

        // Reminder: You won't need to annotate the "concert" argument above - arguments without annotations are
        // assumed by JAX-RS to come from the HTTP request body.

        NewCookie newCookie = makeCookie(clientId);
        long id = idCounter.getAndAdd(1);
        concertDB.put(id, concert);

        String location = Config.WEB_SERVICE_URI + "/" + id;

        // location should be the entire URL
        return Response.ok().status(Response.Status.CREATED).cookie(newCookie).header("Location", location).build();
    }

    @DELETE
    public Response deleteAllConcerts(@CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {

        NewCookie newCookie = makeCookie(clientId);
        concertDB.clear();

        return Response.ok().status(Response.Status.NO_CONTENT).cookie(newCookie).build();
    }

    /**
     * Helper method that can be called from every service method to generate a
     * NewCookie instance, if necessary, based on the clientId parameter.
     *
     * @param clientId the Cookie whose name is Config.CLIENT_COOKIE, extracted
     *                 from a HTTP request message. This can be null if there was no cookie
     *                 named Config.CLIENT_COOKIE present in the HTTP request message.
     * @return a NewCookie object, with a generated UUID value, if the clientId
     * parameter is null. If the clientId parameter is non-null (i.e. the HTTP
     * request message contained a cookie named Config.CLIENT_COOKIE), this
     * method returns null as there's no need to return a NewCookie in the HTTP
     * response message.
     */
    private NewCookie makeCookie(Cookie clientId) {
        NewCookie newCookie = null;

        if (clientId == null) {
            newCookie = new NewCookie(Config.CLIENT_COOKIE, UUID.randomUUID().toString());
            LOGGER.info("Generated cookie: " + newCookie.getValue());
        }

        return newCookie;
    }
}
