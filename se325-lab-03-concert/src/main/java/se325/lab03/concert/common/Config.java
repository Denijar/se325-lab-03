package se325.lab03.concert.common;

/**
 * Class with shared configuration data for the client and Web service.
 */
public class Config {
    /**
     * Name of a cookie exchanged by clients and the Web service.
     */
    public static final String CLIENT_COOKIE = "clientId";
    public static String WEB_SERVICE_URI = "http://localhost:10000/services/concerts";
}
